package com.example.mybatishw.repository;

import com.example.mybatishw.model.Enity.Customer;
import com.example.mybatishw.model.CustomerBody;
import org.apache.ibatis.annotations.*;


import java.util.List;

@Mapper

public interface CustomerRepo {


    @Select("""
            SELECT * FROM customer_tb
            """)
    @Result(property = "customerId",column = "customer_id")
    @Result(property = "customerName",column = "customer_name")
    @Result(property = "customerAddress",column = "customer_address")
    @Result(property = "customerPhone",column = "customer_phone")
    List<Customer> getAllCustomer();

    @Select("""
            SELECT * FROM customer_tb WHERE customer_id = #{id};
            """)
    @Result(property = "customerId",column = "customer_id")
    @Result(property = "customerName",column = "customer_name")
    @Result(property = "customerAddress",column = "customer_address")
    @Result(property = "customerPhone",column = "customer_phone")
    Customer getCustomerById(Integer id);

    @Select( """
            INSERT INTO customer_tb(customer_name,customer_address,customer_phone)
            VALUES (#{p.customerName}, #{p.customerAddress},#{p.customerPhone})
            RETURNING *
            """)
    @Result(property = "customerId",column = "customer_id")
    @Result(property = "customerName",column = "customer_name")
    @Result(property = "customerAddress",column = "customer_address")
    @Result(property = "customerPhone",column = "customer_phone")
    Customer insertCustomer(@Param("p") CustomerBody custome);


    @Select("""
                UPDATE customer_tb
                        SET customer_name = #{c.customerName}, customer_address = #{c.customerAddress}, customer_phone = #{c.customerPhone}
                        WHERE customer_id = #{id}
                        RETURNING *
            """)
    @Result(property = "customerId",column = "customer_id")
    @Result(property = "customerName",column = "customer_name")
    @Result(property = "customerAddress",column = "customer_address")
    @Result(property = "customerPhone",column = "customer_phone")
    Customer updateCustomerById(Integer id, @Param("c")  CustomerBody customer);



    @Delete("""
            DELETE FROM customer_tb
            WHERE customer_id = #{id};
            """)
    void deleteCustomerById(Integer id);
}
