package com.example.mybatishw.repository;

import com.example.mybatishw.model.Enity.Product;
import com.example.mybatishw.model.ProductBody;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface ProductRepo {
    @Select("""
            SELECT * FROM product_tb
            """)
   @Result(property = "productId",column = "product_id")
    @Result(property = "productName",column = "product_name")
    @Result(property = "price",column = "product_price")
   List <Product> getAllProduct();

    @Select("""
            SELECT * FROM product_tb WHERE product_id = #{id};
            """)
    @Result(property = "productId" , column = "product_id")
    @Result(property = "productName",column = "product_name")
    @Result(property = "price",column = "product_price")
    Product getProductById(Integer id);


    @Select( """
            INSERT INTO product_tb(product_name,product_price)
            VALUES (#{p.productName}, #{p.price})
            RETURNING *
            """)
    @Result(property = "productId" , column = "product_id")
    @Result(property = "productName",column = "product_name")
    @Result(property = "price",column = "product_price")
    Product insertProduct(@Param("p")  Product product);

    @Delete("""
            DELETE FROM product_tb
            WHERE product_id = #{id};
            
            """)
    void deleteProductById(Integer id);

 @Select("""
                UPDATE product_tb
                        SET product_name = #{p.productName}, product_price =  #{p.price}
                        WHERE product_id = #{id}
                        RETURNING *
            """)
Product updateProductById(Integer id , @Param("p")  ProductBody product);


 @Select("""
         SELECT p.product_id,p.product_name,p.product_price FROM product_tb p
         INNER JOIN invoice_detail_tb idt on p.product_id = idt.product_id
         where invoice_id = #{invoiceId};
         """)
 @Result(property = "productId" , column = "product_id")
 @Result(property = "productName",column = "product_name")
 @Result(property = "price",column = "product_price")
 public List<Product> getALlInvoiceById(Integer invoiceId);

}
