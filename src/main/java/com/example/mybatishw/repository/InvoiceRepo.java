package com.example.mybatishw.repository;

import com.example.mybatishw.model.Enity.Invoice;
import com.example.mybatishw.model.InvoiceBody;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface InvoiceRepo {

    @Select("""
            SELECT * FROM invoice_tb
            """)
    @Result(property = "customer", column = "customer_id",
    one = @One(select = "com.example.mybatishw.repository.CustomerRepo.getCustomerById")
    )
    @Result(property ="invoiceId",column = "invoice_id")
    @Result(property = "timestamp",column ="invoice_date")
    @Result(property = "products" ,column = "invoice_id",
    many = @Many(select = "com.example.mybatishw.repository.ProductRepo.getALlInvoiceById")
    )
    List<Invoice> getAllInvoice();

    @Select("""
            SELECT * FROM invoice_tb WHERE invoice_id = #{id};
            """)
    @Result(property = "customer", column = "customer_id",
            one = @One(select = "com.example.mybatishw.repository.CustomerRepo.getCustomerById")
    )
    @Result(property ="invoiceId",column = "invoice_id")
    @Result(property = "timestamp",column ="invoice_date")
    @Result(property = "products" ,column = "invoice_id",
            many = @Many(select = "com.example.mybatishw.repository.ProductRepo.getALlInvoiceById")
    )
    Invoice getInvoiceById(Integer id);
    @Select("""
            INSERT INTO invoice_tb(invoice_date,customer_id)
            VALUES(#{invoice.timestamp}, #{invoice.customerId})
            RETURNING invoice_id
            """)
    Integer saveInvoice(@Param("invoice") InvoiceBody invoiceBody);

    @Select("""
            INSERT INTO invoice_detail_tb(invoice_id,product_id)
            VALUES(#{invoiceId}, #{productId})
            """)
    Integer invoiceDetail(Integer invoiceId, Integer productId);

    @Delete("""
            DELETE FROM invoice_tb
            WHERE  invoice_id = #{id};
            """)
    void deleteProductById(Integer id);

    @Select("""
             UPDATE invoice_tb
                        SET customer_id = #{u.customerId}
                        WHERE invoice_id = #{id}
                        RETURNING invoice_id 
            """)
    Integer updateInvoiceById(Integer id,@Param("u") InvoiceBody invoiceBody);


    @Select("""
            UPDATE invoice_detail_tb
                        SET  product_id = #{productId}
                        WHERE id = #{id}
            """)
    Integer updateInvoiceDetail(Integer productId, Integer id);


}
