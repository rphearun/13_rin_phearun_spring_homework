package com.example.mybatishw.controller;

import com.example.mybatishw.model.Enity.Customer;
import com.example.mybatishw.model.CustomerBody;
import com.example.mybatishw.model.respon.ApiRespon;
import com.example.mybatishw.service.CustomerService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/customer")
public class CustomerController {
    private final CustomerService customerService;

    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }
    @GetMapping("/get-all-customer")
    public ResponseEntity<?> getAllCustomer(){
        return ResponseEntity.ok(new ApiRespon<List<Customer>>(
                customerService.getAllCustomer(),
                "Successfully fetch customer",
                true

        ));

    }
    @GetMapping("/get-customer-by-id {Id}")
    public ResponseEntity<?> getCustomerById(@PathVariable("Id") Integer id){
        if(customerService.getCustomerById(id)!=null){
            ApiRespon<Customer> respon =new ApiRespon<>(
                    customerService.getCustomerById(id),
                    "This product was successfully",
                    true

            );
            return ResponseEntity.ok(respon);
        }
        return new ResponseEntity<>("ID Not found ", HttpStatus.NOT_FOUND);
    }
    @PostMapping("/add-new-customer")
    public ResponseEntity<?> insertCustomer(@RequestBody CustomerBody customer){
        return ResponseEntity.ok(new ApiRespon<Customer>(
                customerService.insertCustomer(customer),
                "Add new product is successfully",
                true

        ));
    }
    @PutMapping("/update-customer-by-id {id}")
    public ResponseEntity<?> updateCustomerById(@PathVariable Integer id, @RequestBody CustomerBody customer){
        if(customerService.getCustomerById(id) != null) {
            customerService.updateCustomerById(id, customer);
            return ResponseEntity.ok(new ApiRespon<>(
                    customerService.getCustomerById(id),
                    "Congratulations you update customer by id successfully...!",
                    true
            ));
        }

        return new ResponseEntity<>("ID Not found ", HttpStatus.NOT_FOUND);
    }
    @DeleteMapping("/delete-customer-by-id {Id}")
    public ResponseEntity<?> deleteCustomerById(@PathVariable("Id") Integer id){
        if (customerService.getCustomerById(id)!=null){
            customerService.deleteCustomerById(id);
            return ResponseEntity.ok(new ApiRespon<Customer>((
                    null),
                    "Delete Successfully",
                    true

            ));
        }
        return new ResponseEntity<>("ID Not found ", HttpStatus.NOT_FOUND);
    }
}
