package com.example.mybatishw.controller;

import com.example.mybatishw.model.respon.ApiRespon;
import com.example.mybatishw.model.Enity.Product;
import com.example.mybatishw.model.ProductBody;
import com.example.mybatishw.service.ProductService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/product")
public class
ProductController {
    private final ProductService productService ;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping("/get-all-products")
    public ResponseEntity<?> getAllProduct(){
        return ResponseEntity.ok(new ApiRespon<List<Product>>(
                productService.getAllProduct(),
                "Successfully fetch product",
                true

        ));
    }
    @GetMapping("/get-product-by-id {Id}")
    public ResponseEntity<?> getProductById(@PathVariable("Id") Integer id){
        if (productService.getProductById(id)!=null){
            Product product = productService.getProductById(id);
            ApiRespon<Product> respon =new ApiRespon<>(
                    product,
                    "This product was successfully",
                    true

            );
            return ResponseEntity.ok(respon);
        }
        return new ResponseEntity<>("ID Not found ", HttpStatus.NOT_FOUND);

    }

    @PostMapping("/add-new-product")
    public ResponseEntity<?> insertProduct(@RequestBody Product product){
        return ResponseEntity.ok(new ApiRespon<Product>(
                productService.insertProduct(product),
                "Add new product is successfully",
                true

        ));
    }
    @DeleteMapping("/delete-product-by-id {Id}")
    public ResponseEntity<?> deleteProductById(@PathVariable("Id") Integer id){
        if (productService.getProductById(id)!=null){
            productService.deleteProductById(id);
            return ResponseEntity.ok(new ApiRespon<Product>((
                    null),
                    "Delete Successfully",
                    true

            ));
        }
        return new ResponseEntity<>("ID Not found ", HttpStatus.NOT_FOUND);

    }

    @PutMapping("/update-product-by-id {id}")
    public ResponseEntity<?> updateProductById(@PathVariable Integer id, @RequestBody ProductBody product){
        if(productService.getProductById(id) != null) {
            productService.updateProductById(id, product);
            return ResponseEntity.ok(new ApiRespon<Product>(
                    productService.getProductById(id),
                    "Congratulations you update product by id successfully...!",
                    true

            ));
        }
        return new ResponseEntity<>("cannot find ID ", HttpStatus.NOT_FOUND);

    }
    }

