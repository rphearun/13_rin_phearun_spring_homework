package com.example.mybatishw.controller;

import com.example.mybatishw.model.Enity.Customer;
import com.example.mybatishw.model.Enity.Invoice;
import com.example.mybatishw.model.Enity.Product;
import com.example.mybatishw.model.InvoiceBody;
import com.example.mybatishw.model.respon.ApiRespon;
import com.example.mybatishw.service.InvoiceService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/invoice")
public class InvoiceController {

    private final InvoiceService invoiceService;

    public InvoiceController(InvoiceService invoiceService) {
        this.invoiceService = invoiceService;
    }

    @GetMapping("/get-all-invoice")
    public ResponseEntity<?> getAllInvoic(){
        return ResponseEntity.ok(new ApiRespon<List<Invoice>>(
            invoiceService.getAllInvoice(),
                "Successfully fetch customer",
                true
        ));
    }
    @PostMapping("/get-new-invoice")
    public ResponseEntity<ApiRespon<Invoice>> addNewInvoice(@RequestBody InvoiceBody invoiceBody){
        Integer storeInvoice = invoiceService.addNewInvoice(invoiceBody);
        if (storeInvoice != null){
            return ResponseEntity.ok(new ApiRespon<Invoice>(
                    invoiceService.getInvoiceById(storeInvoice),
                    "add new invoice successfully",
                    true
            ));
        }
        return null;
    }

    @GetMapping("/get-invoice-by-id{id}")
    public ResponseEntity<?> getInvoiceById(@PathVariable Integer id){
        if(invoiceService.getInvoiceById(id)!=null){
            ApiRespon<Invoice> respon =new ApiRespon<>(
                    invoiceService.getInvoiceById(id),
                    "This product was successfully",
                    true
            );
            return ResponseEntity.ok(respon);
        }
        return new ResponseEntity<>("cannot find this ID  ", HttpStatus.NOT_FOUND);
    }
    @DeleteMapping("/delete-invoice-by-id{id}")
    public ResponseEntity<?> deleteInvoiceById(@PathVariable Integer id){
        if (invoiceService.getInvoiceById(id)!=null){
           invoiceService.deleteProductById(id);
            return ResponseEntity.ok(new ApiRespon<Product>((
                    null),
                    "Delete Successfully",
                    true
            ));
        }
        return new ResponseEntity<>("cannot find this ID ", HttpStatus.NOT_FOUND);
    }
    @PutMapping("/update-invoice-by-id{id}")
    public ResponseEntity<?> updateInvoiceById(@PathVariable Integer id, @RequestBody InvoiceBody invoiceBody ){
        if (invoiceService.getInvoiceById(id)!=null){
            invoiceService.updateInvoiceById(id, invoiceBody);
            return ResponseEntity.ok(new ApiRespon<Invoice>(
                    invoiceService.getInvoiceById(id),
                    "Update successfully",
                    true
            ));
        }
        return new ResponseEntity<>("cannot find this ID ", HttpStatus.NOT_FOUND);

    }
}
