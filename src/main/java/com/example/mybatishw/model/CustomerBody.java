package com.example.mybatishw.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CustomerBody {
    private String customerName;
    private String customerAddress;
    private String customerPhone;
}
