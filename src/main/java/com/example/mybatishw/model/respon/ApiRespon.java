package com.example.mybatishw.model.respon;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ApiRespon<T> {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private T payload ;
    private String message;

    @JsonInclude(JsonInclude.Include.NON_NULL)

    private boolean success;
}
