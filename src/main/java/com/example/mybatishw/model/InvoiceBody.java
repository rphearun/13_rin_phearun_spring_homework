package com.example.mybatishw.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.List;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class InvoiceBody {
    private Timestamp timestamp;
    private Integer customerId;
    private List<Integer> productId;

}
