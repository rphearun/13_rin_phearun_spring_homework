package com.example.mybatishw.model.Enity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Invoice {
    private int invoiceId;
    private Timestamp timestamp;
    private Customer customer;
    private List<Product> products;

}
