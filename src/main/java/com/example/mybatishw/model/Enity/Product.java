package com.example.mybatishw.model.Enity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Product {
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Integer productId;
    private String productName;
    private Double price;
}
