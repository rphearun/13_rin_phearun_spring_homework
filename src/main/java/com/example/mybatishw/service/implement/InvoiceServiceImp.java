package com.example.mybatishw.service.implement;

import com.example.mybatishw.model.Enity.Invoice;
import com.example.mybatishw.model.InvoiceBody;
import com.example.mybatishw.repository.InvoiceRepo;
import com.example.mybatishw.service.InvoiceService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InvoiceServiceImp implements InvoiceService {

  private final InvoiceRepo invoiceRepo;

    public InvoiceServiceImp(InvoiceRepo invoiceRepo) {
        this.invoiceRepo = invoiceRepo;
    }
    @Override
    public List<Invoice> getAllInvoice() {
        return invoiceRepo.getAllInvoice() ;
    }



    @Override
    public Invoice getInvoiceById(Integer id) {
        return invoiceRepo.getInvoiceById(id);
    }

    @Override
    public void deleteProductById(Integer id) {
        invoiceRepo.deleteProductById(id);
    }

    @Override
    public Integer addNewInvoice(InvoiceBody invoiceBody) {
        Integer storeInvoiceId =invoiceRepo.saveInvoice(invoiceBody);
        for (Integer productId :invoiceBody.getProductId()){
            invoiceRepo.invoiceDetail(storeInvoiceId,productId);
        }
        return storeInvoiceId;
    }

    @Override
    public Integer updateInvoiceById(Integer id, InvoiceBody invoiceBody) {
        Integer storeUpdateId = invoiceRepo.updateInvoiceById(id,invoiceBody);
        for (Integer productId : invoiceBody.getProductId()) {
            invoiceRepo.updateInvoiceDetail(storeUpdateId,productId);
        }
        return storeUpdateId;
    }
}
