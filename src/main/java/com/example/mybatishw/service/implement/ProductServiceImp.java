package com.example.mybatishw.service.implement;

import com.example.mybatishw.model.Enity.Product;
import com.example.mybatishw.model.ProductBody;
import com.example.mybatishw.repository.ProductRepo;
import com.example.mybatishw.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceImp implements ProductService {
        private ProductRepo productRepo;

    @Autowired
    public void setProductRepo(ProductRepo productRepo) {
        this.productRepo = productRepo;
    }

    @Override
    public List<Product> getAllProduct() {
        return  productRepo.getAllProduct();
    }

    @Override
    public Product getProductById(Integer id) {
        return productRepo.getProductById(id);
    }

    @Override
    public Product insertProduct(Product product) {
        return productRepo.insertProduct(product);
    }

    @Override
    public void deleteProductById(Integer id) {
            productRepo.deleteProductById(id);
    }

    @Override
    public Product updateProductById(Integer id, ProductBody product) {
           return productRepo.updateProductById(id,product);
    }
}
