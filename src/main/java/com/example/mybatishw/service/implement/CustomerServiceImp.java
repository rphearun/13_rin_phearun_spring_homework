package com.example.mybatishw.service.implement;

import com.example.mybatishw.model.Enity.Customer;
import com.example.mybatishw.model.CustomerBody;
import com.example.mybatishw.repository.CustomerRepo;
import com.example.mybatishw.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service

public class CustomerServiceImp implements CustomerService {

    private CustomerRepo customerRepo ;

    @Autowired
    public void setCustomerRepo(CustomerRepo customerRepo) {
        this.customerRepo = customerRepo;
    }

    @Override
    public Customer insertCustomer(CustomerBody customer) {
        return customerRepo.insertCustomer(customer);
    }

    @Override
    public List<Customer> getAllCustomer() {
        return customerRepo.getAllCustomer();
    }

    @Override
    public Customer getCustomerById(Integer id) {
        return customerRepo.getCustomerById(id);
    }

    @Override
    public Customer updateCustomerById(Integer id, CustomerBody customer) {
        return customerRepo.updateCustomerById(id, customer);
    }

    @Override
    public void deleteCustomerById(Integer id) {
        customerRepo.deleteCustomerById(id);
    }
}
