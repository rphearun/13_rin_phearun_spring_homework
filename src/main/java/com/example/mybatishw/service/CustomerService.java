package com.example.mybatishw.service;


import com.example.mybatishw.model.Enity.Customer;
import com.example.mybatishw.model.CustomerBody;

import java.util.List;

public interface CustomerService  {

     Customer insertCustomer(CustomerBody customer) ;


     List<Customer> getAllCustomer();

     Customer getCustomerById(Integer id);


     Customer updateCustomerById(Integer id, CustomerBody customer);

     void deleteCustomerById(Integer id);
}
