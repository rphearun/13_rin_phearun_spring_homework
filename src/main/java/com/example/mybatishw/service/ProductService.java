package com.example.mybatishw.service;

import com.example.mybatishw.model.Enity.Product;
import com.example.mybatishw.model.ProductBody;

import java.util.List;

public interface ProductService {
    List<Product>  getAllProduct();

    Product getProductById (Integer id);

    Product insertProduct(Product product);

      void deleteProductById(Integer id);

    Product updateProductById(Integer id, ProductBody product);
}
