package com.example.mybatishw.service;

import com.example.mybatishw.model.Enity.Invoice;
import com.example.mybatishw.model.InvoiceBody;

import java.util.List;

public interface InvoiceService {
    List<Invoice> getAllInvoice();


    Invoice getInvoiceById(Integer id);

    void deleteProductById(Integer id);

    Integer addNewInvoice(InvoiceBody invoiceBody);

    Integer updateInvoiceById(Integer id, InvoiceBody invoiceBody );

}
