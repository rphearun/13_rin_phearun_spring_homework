create table product_tb
(
    product_id   serial
        primary key,
    product_name  varchar(200) not null,
    product_price numeric      not null
);
create table customer_tb
(
    customer_id      serial
        primary key,
    customer_name    varchar(200) not null,
    customer_address varchar(200) not null,
    customer_phone  varchar(50)     not null
);
create table invoice_tb
(
    invoice_id   serial
        primary key,
    invoice_date timestamp ,
    customer_id  integer not null
        references customer_tb
            on delete cascade on UPDATE cascade
);
create table invoice_detail_tb
(
    id         serial
        primary key,
    invoice_id integer not null
        references invoice_tb
            on delete cascade,
    product_id integer not null
        references product_tb
            on delete cascade
);
